<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rentdetail extends Model
{
    protected $fillable = [
        'rent_id', 'category', 'transaction', 'nominal',
    ];

    public function rent()
	{
		return $this->belongsTo(Rent::class);
	}
}
