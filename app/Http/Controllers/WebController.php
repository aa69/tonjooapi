<?php

namespace App\Http\Controllers;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Image;
use Purifier;

class WebController extends Controller {

	public function index($name=null, $id=null) 
	{
		$setting = Setting::first(['meta_title', 'meta_keywords', 'meta_description']);
		return view('welcome', compact('setting'));
	}

}
