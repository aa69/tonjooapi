<?php

namespace App\Http\Controllers\api;

use App\Rent;
use App\Rentdetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Purifier;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->get('limit')?$request->get('limit'):1;

        $data['transactions'] = Rent::with(['rentdetail'])
                                ->leftJoin('rentdetails', 'rents.id', '=', 'rentdetails.rent_id')
                                ->orderBy('rents.date_paid', 'desc')
                                ->when(!empty($request->get('start_date') || $request->get('end_date')) , function ($query) use($request){
                                    $start_date = $request->get("start_date") ? $request->get("start_date") : date('Y-m-d');
                                    $end_date   = $request->get("end_date") ? $request->get("end_date") : date('Y-m-d');
                                    return $query->whereBetween('rents.date_paid', [$start_date,$end_date]);
                                })
                                ->when(!empty($request->get('category')) , function ($query) use($request){
                                    return $query->where('rentdetails.category', $request->get('category'));
                                })
                                ->when(!empty($request->get("search")) , function ($query) use($request){
                                    $query->where(function($q) use($request){
                                        return $q->where('rentdetails.category', 'like', '%'.$request->get("search").'%')
                                            ->orWhere('rentdetails.transaction', 'like', '%'.$request->get("search").'%')
                                            ->orWhere('rents.description', 'like', '%'.$request->get("search").'%')
                                            ->orWhere('rents.code', 'like', '%'.$request->get("search").'%');
                                    });
                                })
                                ->select(array('rents.description','rents.code','rents.rate_euro','rents.date_paid','rentdetails.category','rentdetails.transaction','rentdetails.nominal', 'rentdetails.id as rentdetail_id', 'rents.id')
                                )
                                ->paginate($limit);

        return $data;
    }

    public function recap(Request $request)
    {
        $limit = $request->get('limit')?$request->get('limit'):10;

        $data['transactions'] = Rent::leftJoin('rentdetails', 'rents.id', '=', 'rentdetails.rent_id')
                                ->orderBy('rents.date_paid', 'desc')
                                ->when(!empty($request->get('start_date') || $request->get('end_date')) , function ($query) use($request){
                                    $start_date = $request->get("start_date") ? $request->get("start_date") : date('Y-m-d');
                                    $end_date   = $request->get("end_date") ? $request->get("end_date") : date('Y-m-d');
                                    return $query->whereBetween('rents.date_paid', [$start_date,$end_date]);
                                })
                                ->when(!empty($request->get('category')) , function ($query) use($request){
                                    return $query->where('rentdetails.category', $request->get('category'));
                                })
                                ->when(!empty($request->get("search")) , function ($query) use($request){
                                    $query->where(function($q) use($request){
                                        return $q->where('rentdetails.category', 'like', '%'.$request->get("search").'%');
                                    });
                                })
                                ->select(array('rents.date_paid','rentdetails.category', DB::raw('SUM(rentdetails.nominal) as total')))
                                ->groupBy('rents.date_paid','rentdetails.category')
                                ->paginate($limit);
        return $data;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description'   => 'required',
            'code'          => 'required|integer|unique:rents',
            'rate_euro'     => 'required',
            'date_paid'     => 'required',
        ], [
            'code.unique'           => 'Code sudah ada.',
        ]);

        if ($validator->passes()) {
            $rent = new Rent();
            $rent->user_id          = Auth::user()->id;
            $rent->description      = $request->get('description');
            $rent->code             = $request->get('code');
            $rent->rate_euro        = $request->get('rate_euro');
            $rent->date_paid        = $request->get('date_paid');

            $affected_row = $rent->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => 'success', 'message' => 'Data berhasil ditambah.', 'rent_id' => $rent->id]);
            } else {
                return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
            }
        }
        return Response::json(['status' => 'errors','message' => $validator->errors()]);
    }

    public function store_detail_transaction(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'       => 'required',
            'category'      => 'required',
            'transaction'   => 'required',
            'nominal'       => 'required',
        ]);

        if ($validator->passes()) {
            $detail_rent = new Rentdetail();
            $detail_rent->rent_id        = $request->get('id');
            $detail_rent->category       = $request->get('category');
            $detail_rent->transaction    = $request->get('transaction');
            $detail_rent->nominal        = $request->get('nominal');
            
            $affected_row = $detail_rent->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => 'success', 'message' => 'Data transaksi berhasil diupdate.']);
            } else {
                return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
            }
        }
        return Response::json(['status' => 'errors', 'message' => $validator->errors()]);
    }

    public function update_detail_transaction(Request $request, $id)
    {
        $detail_rent = Rentdetail::find($id);

        $validator = Validator::make($request->all(), [
            'category'      => 'required',
            'transaction'   => 'required',
            'nominal'       => 'required',
        ]);

        if ($validator->passes()) {
            $detail_rent->category       = $request->get('category');
            $detail_rent->transaction    = $request->get('transaction');
            $detail_rent->nominal        = $request->get('nominal');
            
            $affected_row = $detail_rent->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => 'success', 'message' => 'Data transaksi berhasil diupdate.']);
            } else {
                return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
            }
        }
        return Response::json(['status' => 'errors', 'message' => $validator->errors()]);
    }

    public function destroy_detail_transaction($id)
    {
        $detail_rent = Rentdetail::find($id);
        if ($detail_rent != null) {
            $detail_rent->delete();
            return Response::json(['status' => 'success', 'message' => 'Data transaksi berhasil didelete.']);
        } else {
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rent = Rent::find($id);

        if ($rent->code == $request->input('code')) {
            $code = 'required|integer';
        } else {
            $code = 'required|integer|unique:rents';
        }

        $validator = Validator::make($request->all(), [
            'description'   => 'required',
            'code'          => $code,
            'rate_euro'     => 'required',
            'date_paid'     => 'required',
        ], [
            'code.unique'           => 'Code sudah ada.',
        ]);

        if ($validator->passes()) {
            $rent->description      = $request->get('description');
            $rent->code             = $request->get('code');
            $rent->rate_euro        = $request->get('rate_euro');
            $rent->date_paid        = $request->get('date_paid');

            $affected_row = $rent->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => 'success', 'message' => 'Data berhasil diupdate.']);
            } else {
                return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
            }
        }
        return Response::json(['status' => 'errors','message' => $validator->errors()]);
    }

    public function destroy_transaction($id)
    {
        $detail_rent = Rentdetail::find($id);
        if ($detail_rent != null) {
            $detail_rent->delete();
            return Response::json(['status' => 'success', 'message' => 'Data transaksi berhasil didelete.']);
        } else {
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $detail_rent = Rentdetail::find($id);
        if ($detail_rent != null) {
            $detail_rent->delete();
            $header = Rentdetail::where('rent_id',$detail_rent->rent_id)->get();
            if(count($header) == 0){
                $this->destroy_header_transaction($detail_rent->rent_id);
            }
            return Response::json(['status' => 'success', 'message' => 'Data transaksi berhasil didelete.']);
        } else {
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }
    }

    public function destroy_header_transaction($id)
    {
        $headerData = Rent::find($id);
        if ($headerData != null) {
            $headerData->delete();
            return Response::json(['status' => 'success', 'message' => 'Data transaksi berhasil didelete.']);
        } else {
            return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !']);
        }
    }
}
