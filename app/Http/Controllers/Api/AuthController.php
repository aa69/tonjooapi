<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use JWTAuth;
use Validator;
use App\User;
use Illuminate\Support\Facades\Response;

class AuthController extends Controller
{
    /**
     * Register a new user
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nik'       => 'required|min:15|unique:users',
            'username'  => 'required|min:3|unique:users',
            'name'      => 'required|min:3',
            'email'     => 'required|email|unique:users',
            'password'  => 'required|min:3|confirmed',
        ]);

        if ($validator->passes()) {
            $user = new User();
            $user->usergroup_id         = 2;
            $user->nik                  = $request->nik;
            $user->username             = $request->username;
            $user->name                 = $request->name;
            $user->email                = $request->email;
            $user->password             = bcrypt($request->password);
            $affected_row = $user->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => 'success', 'message' => 'User update successfully.'], 200);
            } else {
                return Response::json(['status' => 'errors', 'message' => 'Operasi gagal !'], 422);
            }
        }
        return Response::json(['status' => 'errors', 'message' => $validator->errors()], 422);


    }
    /**
     * Login user and return a token
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username'      => 'required',
            'password'      => 'required|min:3',
        ]);

        if ($validator->passes()) {
            $credentials = ['username' => $request->username, 'password' => $request->password, 'activation_status' => 1];
            // $credentials = $request->only('email', 'password');
            if ($token = $this->guard()->attempt($credentials)) {
                // dd(Auth::user()->activation_status);
                // return response()->json(['status' => 'success'], 200)->header('Authorization', $token);
                return Response::json(['status' => 'success', 'message' => 'Selamat datang '], 200)
                        ->header('Authorization', $token)
                        ->header('Access-Control-Allow-Origin', '*')
                        ->header('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With')
                        ->header('Access-Control-Expose-Headers', 'Authorization')
                        ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
            }
            // return response()->json(['error' => 'login_error'], 401);
            return Response::json(['status' => 'errors', 'message' => 'Username atau Password salah'], 401);
        }
        return Response::json(['status' => 'errors', 'message' => $validator->errors()], 422);
    }
    /**
     * Logout User
     */
    public function logout()
    {
        $this->guard()->logout();
        return response()->json([
            'status' => 'success',
            'msg' => 'Logged out Successfully.'
        ], 200);
    }
    /**
     * Get authenticated user
     */
    public function user(Request $request)
    {
        $user = User::find(Auth::user()->id);
        return response()->json([
            'status' => 'success',
            'data' => $user
        ]);
    }
    /**
     * Refresh JWT token
     */
    public function refresh()
    {
        if ($token = $this->guard()->refresh()) {
            return response()
                ->json(['status' => 'successs'], 200)
                ->header('Authorization', $token)
                ->header('Access-Control-Allow-Origin', '*')
                ->header('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With')
                ->header('Access-Control-Expose-Headers', 'Authorization')
                ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
        }
        return response()->json(['error' => 'refresh_token_error'], 401);
    }
    /**
     * Return auth guard
     */
    private function guard()
    {
        return Auth::guard();
    }
}
