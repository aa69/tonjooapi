<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rent extends Model
{
    protected $fillable = [
        'user_id', 'description', 'code', 'rate_euro', 'date_paid',
    ];

    public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function rentdetail()
	{
		return $this->hasMany(Rentdetail::class);
	}

	public function rentdetails()
	{
	    return $this->rentdetails();
	}
}
