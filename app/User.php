<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject{
	use Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'usergroup_id', 'name', 'username', 'email', 'password', 'avatar', 'gender', 'phone', 'address', 'tgl_lahir', 'agama', 'kode_pos', 'facebook', 'twitter', 'google_plus', 'linkedin', 'about', 'role', 'activation_status','web_tour',

	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

 	public static function create(Request $request)
    {
        $user = new User();
        if (!empty($request->get('username'))) {
            $user->username = $request->get('username');
        }
        if (!empty($request->get('password'))) {
            $user->password = bcrypt($request->get('password'));
        }
        $user->save();
        return $user;
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    
    public function getJWTCustomClaims()
    {
        return [];
    }

	public function getAuthPassword()
	{
	    return $this->password;
	}

	public function usergroup() {
		return $this->belongsTo(Usergroup::class);
	}

	public function rent() {
		return $this->hasMany(Rent::class);
	}
}
