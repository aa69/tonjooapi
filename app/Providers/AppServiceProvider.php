<?php

namespace App\Providers;

use App\Setting;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['web.includes.header', 'web.includes.footer', 'admin.includes.header', 'admin.includes.footer', 'web.includes.head', 'admin.includes.head', 'auth.login'], function ($view) {
            $setting = Setting::first();
            $view->with(compact('setting'));
        });
    }
}
