<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => '/', 'namespace' => 'Api', 'as' => 'api.'], function () {
    // transaksi
    Route::resource('apitransaction', 'TransactionController', ['only' => ['index', 'show']]);
    Route::get('apirecaptransaction', 'TransactionController@recap');
    Route::get('apidashboard', 'WebController@index');

    Route::prefix('auth')->group(function () {

	    Route::post('register', 'AuthController@register');
	    // Login User
	    Route::post('login', 'AuthController@login');
	    // Refresh the JWT Token
	    Route::get('refresh', 'AuthController@refresh');
	    
	    // Below mention routes are available only for the authenticated users.
	    Route::middleware('auth:api')->group(function () {
	        // Get user info
	        Route::get('user', 'AuthController@user');
	        // Logout user from application
	        Route::post('logout', 'AuthController@logout');

            // transaksi
            Route::resource('apitransaction', 'TransactionController', ['except' => ['index', 'show']]);
            Route::delete('apiheadertransaction/{id}', 'TransactionController@destroy_header_transaction');

            // detail transaksi
            Route::post('apidetailtransaction', 'TransactionController@store_detail_transaction');
            Route::patch('apidetailtransaction/{id}', 'TransactionController@update_detail_transaction');
            Route::delete('apidetailtransaction/{id}', 'TransactionController@destroy_detail_transaction');
	    });
    });
});
