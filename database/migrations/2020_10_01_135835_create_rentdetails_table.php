<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rentdetails', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rent_id')->unsigned()->index();

            $table->string('category');
            $table->string('transaction')->nullable();
            $table->integer('nominal')->nullable();
            $table->timestamps();
            
            $table->foreign('rent_id')->references('id')->on('rents')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rentdetails');
    }
}
