<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('settings')->insert([
            [
            	'id' => 1,
				'website_title' => 'tonjoo',
				'logo' => 'tonjoo.png',
				'favicon' => 'tonjoo.png',
				'about_us' => 'tonjoo.',
				'copyright' => 'Copyright 2020 <a href="#" target="_blank">tonjoo</a>, All rights reserved.',
				'email' => 'tonjoo@tonjoo.com',
				'phone' => '628998897943',
				'mobile' => '+628998897943',
				'whatsapp' => '998897943',
				'fax' => '',
				'address_line_one' => '',
				'address_line_two' => '',
				'state' => '',
				'city' => '',
				'zip' => '',
				'country' => 'Indonesia',
				'map_iframe' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2642905.2881059386!2d89.27605108245604!3d23.817470325158617!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30adaaed80e18ba7%3A0xf2d28e0c4e1fc6b!2sBangladesh!5e0!3m2!1sen!2sbd!4v1520764767552" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
				'facebook' => 'https://facebook.com',
				'twitter' => 'https://twitter.com',
				'google_plus' => 'https://plus.google.com',
				'linkedin' => 'https://www.linkedin.com/',
				'meta_title' => 'travel, tonjoo kendaraan, mobil',
				'meta_keywords' => 'travel, tonjoo kendaraan, mobil',
				'meta_description' => 'travel, tonjoo kendaraan, mobil',
				'gallery_meta_title' => 'travel, tonjoo kendaraan, mobil',
				'gallery_meta_keywords' => 'travel, tonjoo kendaraan, mobil',
				'gallery_meta_description' => 'travel, tonjoo kendaraan, mobil',
            ]
        ]);
    }
}
