<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
    	
    	DB::table('users')->insert([
            [
                'usergroup_id' => 1,
                'name' => 'admin',
                'username' => 'admin',
                'email' => 'admin@mail.com',
                'password' => bcrypt('secret'),
    			'role' => 'admin',
    			'remember_token' => str_random(10),
                'activation_status' => 1,
            ]
        ]);
    }
}
